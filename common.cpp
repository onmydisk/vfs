/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "common.h"
#include <ctype.h>

int getSerialNumber(const QByteArray &raw_sn)
{
    int sn = -1;

    QByteArray parsed_sn;
    //qDebug() << "Raw serial number: " << raw_sn;
    //filter only hex digits
    for (int i = 0; i < raw_sn.size(); i++)
    if (isxdigit(raw_sn[i]))
    {
            parsed_sn.append(raw_sn[i]);
    }
    //convert to int
    bool ok = false;
    int parsed_int = parsed_sn.toInt(&ok, 16);
    if (ok) sn = parsed_int;
    return sn;
}
