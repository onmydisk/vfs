/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#ifndef VFSRESPONSE_H
#define VFSRESPONSE_H

#include <QString>
#include <QStringList>
#include <common.h>
#include <QDataStream>
#include <qstat.h>

class VFSResponse
{
public:
    VFSResponse();
    int opCode;
    int retCode;
    int share_id;    
    VFSFileInfo qStat;
    QList<VFSFileInfo> listing;
    QByteArray data;
    quint64 fh;
    quint64 size;
    quint64 f_bsize;
    quint64 f_bavail;
    quint64 f_bfree;
    quint64 f_blocks;
    friend QDataStream & operator<<(QDataStream & ds, const VFSResponse &VFSResponseData);
    friend QDataStream & operator>>(QDataStream & ds, VFSResponse &VFSResponseData);
};

inline QDataStream & operator<<(QDataStream & ds, const VFSResponse &VFSResponseData)
{
    ds << VFSResponseData.opCode;
    ds << VFSResponseData.retCode;
    ds << VFSResponseData.share_id;
    switch (VFSResponseData.opCode) {
    case CMD_GETATTR:
        ds << VFSResponseData.qStat;
        break;
    case CMD_OPEN:
        ds << VFSResponseData.fh;
        break;
    case CMD_READDIR:
        ds << VFSResponseData.listing;
        break;
    case CMD_READ:
        ds << VFSResponseData.data;
        ds << VFSResponseData.size;
        break;    
    case CMD_STATVFS:
        ds << VFSResponseData.f_bavail;
        ds << VFSResponseData.f_bfree;
        ds << VFSResponseData.f_blocks;
        ds << VFSResponseData.f_bsize;
    break;
    default:
        break;
    }
    return ds;
}

inline QDataStream & operator>>(QDataStream & ds, VFSResponse &VFSResponseData)
{

    //return if no data
    if (ds.atEnd()) return ds;

    ds >> VFSResponseData.opCode;
    ds >> VFSResponseData.retCode;
    ds >> VFSResponseData.share_id;
    switch (VFSResponseData.opCode) {
    case CMD_GETATTR:
        ds >> VFSResponseData.qStat;
        break;
    case CMD_OPEN:
        ds >> VFSResponseData.fh;
        break;
    case CMD_READDIR:
        ds >> VFSResponseData.listing;
        break;
    case CMD_READ:
        ds >> VFSResponseData.data;
        ds >> VFSResponseData.size;
        break;
    case CMD_STATVFS:
        ds >> VFSResponseData.f_bavail;
        ds >> VFSResponseData.f_bfree;
        ds >> VFSResponseData.f_blocks;
        ds >> VFSResponseData.f_bsize;
        break;
    default:
        break;
    }
    return ds;
}

#endif // VFSRESPONSE_H
