/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "blockreader.h"



BlockReader::BlockReader(QIODevice *io, int v)
{
    buffer.open(QIODevice::ReadWrite);
    _stream.setVersion(v);
    _stream.setDevice(&buffer);

    quint64 blockSize;

    // Read the size.
    readMax(io, sizeof(blockSize));
    buffer.seek(0);

    _stream >> blockSize;
    // Read the rest of the data.
    readMax(io, blockSize);
    buffer.seek(sizeof(blockSize));
}


QDataStream&  BlockReader::stream()
{
    return _stream;
}


int BlockReader::readMax(QIODevice *io, int n)
{
    int bytes_written = 0;
    while (io->isOpen() && (buffer.size() < n)  ) {
        if (!io->bytesAvailable()) {
            if (!io->waitForReadyRead(3000)){
                qDebug() << "Blockreader timeout.";
                return -1;
            }
        }
        QByteArray bytes = io->read(n - buffer.size());
        if (bytes.count() == 0)
        {
                 qDebug() << "Blockreader error while reading from socket.";
                 return -1;
        }

        int bcount = buffer.write(bytes);
        if (bcount == -1)
        {
             qDebug() << "Blockreader error while writing to buffer.";
             return -1;
        }
        bytes_written += bcount;
       // qDebug() << "buffer size: " << buffer.size() << ", we need: " << n;
    }
    if (!io->isOpen())
    {
       qDebug() << "Socket was closed while reading";
       return -1;
    }
       else return bytes_written;
}
